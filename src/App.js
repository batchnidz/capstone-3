import './App.css';
import Login from './pages/Login'
import AppNavbar from './components/AppNavbar'
import { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { UserProvider } from './UserContext'
import {Container} from 'react-bootstrap'
import Home from './pages/Home'
import Register from './pages/Register'

function App() {

   const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

      useEffect(() => {

      fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`
        }
      })
      .then(res => res.json())
      .then(data => {

       
        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });

       
        } else {

          setUser({
            id: null,
            isAdmin: null
          });

        }

      })

      }, []);

      


  return (
    <>

    <UserProvider value={{user, setUser, unsetUser}}>
     <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
            {/*  <Route path="/courses" element={<Courses/>}/>
              <Route path="/courses/:courseId" element={<CourseView/>}/>*/}
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
          {/*    <Route path="/logout" element={<Logout/>} />
              <Route path="/*" element={<PageNotFound/>}/>*/}
            </Routes>
          </Container>
        </Router>
    </UserProvider>
    </>
  );
}

export default App;

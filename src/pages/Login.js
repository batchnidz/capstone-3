import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
  
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const navigate = useNavigate()

  
    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {

            
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    function authenticate(event){
        event.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            if(typeof result.access !== "undefined"){
                localStorage.setItem('token', result.access)

                retrieveUser(result.access)

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to HipHopShop!'
                })
                navigate('/')
            } else {
                Swal.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: 'Invalid Email or password'
                })
            }
        })
    }



    

    useEffect(() => {
        if((email !== '' && password !== '')){
            
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) ?
            <Navigate to="/"/>
        :  
            <Form className="form-container" onSubmit={event => authenticate(event)}>
                <Form.Group controlId="userEmail">
                    <Form.Label className="frmLg" >Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={event => setEmail(event.target.value)}
                        required
                    />
                   
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label className="frmLg" >Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={event => setPassword(event.target.value)}
                        required
                    />
                </Form.Group>

                {   isActive ?
                    <Button variant="success" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="success" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }



                
            </Form>
    )
}

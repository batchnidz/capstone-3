import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {
	
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()



	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')


	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email already exist!'
				})
			} else {
				
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNumber: mobileNumber,
						email:email,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					console.log(result);
					
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNumber('');

					if(result){
						Swal.fire({
							title: 'Register Successful!',
							icon: 'success',
							text: 'Thank you and Enjoy!'
							})
						
						navigate('/login')

					} else {
						Swal.fire({	
							title: 'Registration Failed',
							icon: 'error',
							text: "There is Something wrong! :("
						})
					}		
				})
			}
		})

	}
	

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])

	return (
		(user.id !== null) ?
			<Navigate to="/"/>
		:
			<Form className="formReg" onSubmit={event => registerUser(event)}>

				<Form.Group className="formGrpReg" controlId="firstName">
			        <Form.Label className="frmTxt">First Name</Form.Label>
				        <Form.Control className="formCntrl"
				            type="text" 
				            placeholder="Enter First Name"
				            value={firstName} 
				            onChange={event => setFirstName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group className="formGrpReg" controlId="lastName">
			        <Form.Label className="frmTxt">Last Name</Form.Label>
				        <Form.Control className="formCntrl"
				            type="text" 
				            placeholder="Enter Last Name"
				            value={lastName} 
				            onChange={event => setLastName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group className="formGrpReg" controlId="mobileNumber">
			        <Form.Label className="frmTxt">Mobile Number</Form.Label>
				        <Form.Control className="formCntrl" 
				            type="text" 
				            placeholder="Enter Mobile Number"
				            value={mobileNumber} 
				            onChange={event => setMobileNumber(event.target.value)}
				            required
				        />
			    </Form.Group>

			   	<Form.Group className="formGrpReg" controlId="userEmail">
			        <Form.Label className="frmTxt">Email address</Form.Label>
				        <Form.Control className="formCntrl"
				            type="email" 
				            placeholder="Enter email"
				            value={email} 
				            onChange={event => setEmail(event.target.value)}
				            required
				        />
			        
			    </Form.Group>

	            <Form.Group className="formGrpReg" controlId="password1">
	                <Form.Label className="frmTxt">Password</Form.Label>
	                <Form.Control className="formCntrl"
		                type="password" 
		                placeholder="Password" 
		                value={password1} 
				        onChange={event => setPassword1(event.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group className="formGrpReg" controlId="password2">
	                <Form.Label className="frmTxt">Verify Password</Form.Label>
	                <Form.Control className="formCntrl"
		                type="password" 
		                placeholder="Verify Password" 
		                value={password2} 
				        onChange={event => setPassword2(event.target.value)}
		                required
	                />
	            </Form.Group>

	            {	isActive ? 
	            		<Button className="butReg" variant="success" type="submit" id="submitBtn">
			            	Submit
			            </Button>
			            :
			            <Button className="butReg" variant="success" type="submit" id="submitBtn" disabled>
			            	Submit
			            </Button>
	            }
	            
			</Form>
	)
}